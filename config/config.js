module.exports = {
  development: {
    app: {
      name: 'SAML example',
      port: process.env.PORT || 3000
    },
    passport: {
      strategy: 'saml',
      saml: {
        path: process.env.SAML_PATH || '/login/callback',
        entryPoint: process.env.SAML_ENTRY_POINT || 'https://saml-idp.usgs.mobomo.net/',
        issuer: 'accubits-chatbot-sp',
        cert: process.env.SAML_CERT || 'MIIDtDCCApwCCQCF/fogFoI0VTANBgkqhkiG9w0BAQsFADCBmzELMAkGA1UEBhMCVVMxETAPBgNVBAgMCFZpcmdpbmlhMQ8wDQYDVQQHDAZWaWVubmExEzARBgNVBAoMCk1vYm9tbyBMTEMxDTALBgNVBAsMBFVTR1MxITAfBgNVBAMMGHNhbWwtaWRwLnVzZ3MubW9ib21vLm5ldDEhMB8GCSqGSIb3DQEJARYSY2xpbnRvbkBtb2JvbW8uY29tMB4XDTE4MTAwMjAzMTk1MVoXDTIxMTAwMTAzMTk1MVowgZsxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhWaXJnaW5pYTEPMA0GA1UEBwwGVmllbm5hMRMwEQYDVQQKDApNb2JvbW8gTExDMQ0wCwYDVQQLDARVU0dTMSEwHwYDVQQDDBhzYW1sLWlkcC51c2dzLm1vYm9tby5uZXQxITAfBgkqhkiG9w0BCQEWEmNsaW50b25AbW9ib21vLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMW9M13AgY2twlCt6K9PRUByKFi+5OmPG1gbB6gM+MFhS2YMBuJ5mx+H32OeJ/G0vOZirgd2SfvYMDIw+yZlPwKc3Yi14u7LsdHCiccQhFlh06+eFtabUP5TGhd8eruVuOuxeS7W9gHjt1EnGzpeqKVTOAHM6WFMlz7kp1BPi49GaHbjoPw/fqebIAFIOyNgCvzvGGMTJ6yO95CTqOHbE2KeiZUY846Yf0fml8JsAk2xLHs/lz+G/KeyKHkFaR7O8YbUGEhDVwXNTJAP55k+yGBUgKRry9s3Hl52XKUbfOQiA9z3iZbXcvcGLl/66/D1/VTKxJWruDbocvE87Xl5VrkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEApMTwC6B+t4DI3fNdQzuXRThC/vd1DYwDaZdzhBpJtXu+BkcTMSNEaHSbudcxcCAgIcMQW6nqbX0GL0M853e6msB+lZTBuYak2E/v3Bop/caAnugAdJpU1GPifUckg0nfX6gBU/lwCjNZgkiklpQnsVasAhRlPtGZHbAfK5JJGAXCqMnROxZtt6souA1PlCQLZ6C+mzy+v1lw8piTva0A4CVOjBFCD69+hmT2msy1D65+4SKSxyay18+JdAHxNGEGOGknaeejmfyeOU/Ee0Zo2+s6x1DSruBcuQXJIAAvCg/xVr1P2T1IizLXfT1aLytMpT3Qz18Vm3yVRVAkhd2G7w=='
      }
    }
  }
};
